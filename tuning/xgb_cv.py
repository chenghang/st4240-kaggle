import sys
original_path = sys.path

sys.path = original_path
sys.path.append('../py')
from nb_imports import *
from model_params import model_parms, do_not_use_for_training
from derived_features import *

def log_transform(x):
    return np.log(x)
def inv_log_transform(x):
    return np.exp(x)

def mspe_eval(preds, dtrain, transform_f=None):
    _preds = preds
    if type(dtrain) ==  xgb.core.DMatrix:
        labels = dtrain.get_label()
    else:
        labels = dtrain
    if transform_f is not None:
        labels = transform_f(labels)
        _preds = transform_f(_preds)
    # return a pair metric_name, result
    # since preds are margin(before logistic transformation, cutoff at 0)
    return 'mspe', \
        (sum((((_preds)-(labels))/(labels)) ** 2) / len(labels))**0.5

def score_func(y, y_pred):
    return mspe_eval(y_pred, y, transform_f=inv_log_transform)[1]

mspe_scorer = make_scorer(score_func, greater_is_better=False)

train_data = pd.read_parquet('../data/train_basic3.parquet')
train_data.columns = train_data.columns.str.lower()

seed = int(str(time.time()).split('.')[1]) * 2 + int(str(time.time()).split('.')[0]) % 3
rns = [random.randint(0, 1<<31) for i in range(10)]    
open('./seed.txt', 'w').write(str(seed))

valid_split = KFold(n_splits=4, shuffle=True, random_state=rns[1])
from modelling import FeatureExtract

from sklearn.externals.joblib import Memory
from tempfile import mkdtemp
# Create a temporary folder to store the transformers of the pipeline
cachedir = mkdtemp(dir='/mnt/temp')
memory = Memory(cachedir=cachedir, verbose=0)


## XGB

xgb_dur_pipe = Pipeline([
    ('fe', FeatureExtract(do_not_use_for_training, verbose=0)),
    ('xgbr', xgb.XGBRegressor(**model_parms['dur_xgb'])),
], memory=memory)
xgb_len_pipe = Pipeline([
    ('fe', FeatureExtract(do_not_use_for_training, verbose=0)),
    ('xgbr', xgb.XGBRegressor(**model_parms['len_xgb'])),
], memory=memory)



param_test1 = {
'xgbr__max_depth': [9,10,11],
'xgbr__min_child_weight': [7, 8, 9, 10,11],
 'xgbr__gamma' :[i/10.0 for i in range(0,4)]
}
gsearch1 = GridSearchCV(estimator = xgb_dur_pipe, 
                        param_grid = [param_test1],
                        scoring=mspe_scorer,
                        n_jobs=1,
                        iid=False,
                        cv=ShuffleSplit(n_splits=6, test_size=.25, random_state=12345)
                       )

param_test2 = {
'xgbr__max_depth': [10,11,12],
'xgbr__min_child_weight': [10, 11, 12,13, 14],
 'xgbr__gamma':[i/10.0 for i in range(0,4)]
}
gsearch2 = GridSearchCV(estimator = xgb_len_pipe, 
                        param_grid = [param_test2],
                        scoring=mspe_scorer,
                        n_jobs=1,
                        iid=False,
                        cv=ShuffleSplit(n_splits=6, test_size=.25, random_state=12345)
                       )


from sklearn.externals import joblib

print('fitting gsearch1')
gsearch1.fit(train_data, train_data['duration'].apply(np.log1p))
joblib.dump(gsearch1, 'gsearch1.pkl') 
print(gsearch1.best_params_)
print(gsearch1.best_score_)
print(gsearch1.cv_results_)
del gsearch1

print('fitting gsearch2')
gsearch2.fit(train_data, train_data['traj_length'].apply(np.log1p))
joblib.dump(gsearch2, 'gsearch2.pkl') 
print(gsearch2.best_params_)
print(gsearch2.best_score_)
print(gsearch2.cv_results_)

import shutil
shutil.rmtree(cachedir)
