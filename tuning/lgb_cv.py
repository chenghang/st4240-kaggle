#invite people for the Kaggle party
import pandas as pd
import numpy as np
import random
from scipy.stats import norm
from sklearn.preprocessing import StandardScaler
from sklearn.metrics.pairwise import paired_distances
from sklearn.linear_model import ElasticNet, Lasso,  BayesianRidge, LassoLarsIC
from sklearn.ensemble import RandomForestRegressor,  GradientBoostingRegressor, ExtraTreesRegressor
from sklearn.kernel_ridge import KernelRidge
from sklearn.pipeline import make_pipeline, Pipeline
from sklearn.preprocessing import RobustScaler, Imputer
from sklearn.base import BaseEstimator, TransformerMixin, RegressorMixin, clone
from sklearn.model_selection import KFold, cross_val_score, train_test_split
from sklearn import metrics
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import GridSearchCV   #Perforing grid search
from sklearn.model_selection import cross_validate
from sklearn.metrics import make_scorer
import multiprocessing

import sys
sys.path.append('../py')
from nb_imports import *
from modelling import FeatureExtract
from model_params import model_parms, do_not_use_for_training

def log_transform(x):
    return np.log(x)
def inv_log_transform(x):
    return np.exp(x)

def mspe_eval(preds, dtrain, transform_f=None):
    _preds = preds
    if type(dtrain) ==  xgb.core.DMatrix:
        labels = dtrain.get_label()
    else:
        labels = dtrain
    if transform_f is not None:
        labels = transform_f(labels)
        _preds = transform_f(_preds)
    # return a pair metric_name, result
    # since preds are margin(before logistic transformation, cutoff at 0)
    return 'mspe', \
        (sum((((_preds)-(labels))/(labels)) ** 2) / len(labels))**0.5

def score_func(y, y_pred):
    return mspe_eval(y_pred, y, transform_f=inv_log_transform)[1]

mspe_scorer = make_scorer(score_func, greater_is_better=False)

train_data = pd.read_parquet('../data/train_basic3.parquet')
train_data.columns = train_data.columns.str.lower()

seed = int(str(time.time()).split('.')[1]) * 2 + int(str(time.time()).split('.')[0]) % 3
rns = [random.randint(0, 1<<31) for i in range(10)]    

valid_split = KFold(n_splits=4, shuffle=True, random_state=rns[1])


from sklearn.externals.joblib import Memory
from tempfile import mkdtemp
# Create a temporary folder to store the transformers of the pipeline
cachedir = mkdtemp(dir='/mnt/temp')
memory = Memory(cachedir=cachedir, verbose=0)


param_test1 = {
    'lgbr__num_leaves': [500, 1000, 1500, 2000],
    'lgbr__min_child_samples': [10, 25, 60, 150],
#     'lgbr__min_samples_split': [25, 35],
#     'lgbr__max_features' : [0.85, 0.8, 0.75],
}

param_test2 = {
    'lgbr__num_leaves': [500, 1000, 1500, 2000],
    'lgbr__min_child_samples': [10, 25, 60, 150],
#     'lgbr__min_samples_split': [25, 35],
#     'lgbr__max_features' : [0.85, 0.8, 0.75],
}

## ERF
lgb_dur_pipe = Pipeline([
    ('fe', FeatureExtract(do_not_use_for_training, verbose=0)),
    ('lgbr', lgb.LGBMRegressor(**model_parms['dur_lgb'])),
], memory=memory)
lgb_len_pipe = Pipeline([
    ('fe', FeatureExtract(do_not_use_for_training, verbose=0)),
    ('lgbr', lgb.LGBMRegressor(**model_parms['len_lgb'])),
], memory=memory)



gsearch1 = GridSearchCV(estimator = lgb_dur_pipe, 
                        param_grid = [param_test1],
                        scoring=mspe_scorer,
                        n_jobs=1,
                        iid=False,
                        cv=ShuffleSplit(n_splits=6, test_size=.25, random_state=12345)
                       )

gsearch2 = GridSearchCV(estimator = lgb_len_pipe, 
                        param_grid = [param_test2],
                        scoring=mspe_scorer,
                        n_jobs=1,
                        iid=False,
                        cv=ShuffleSplit(n_splits=6, test_size=.25, random_state=12345)
                       )


from sklearn.externals import joblib
import json

print('fitting gsearch1')
gsearch1.fit(train_data, train_data['duration'].apply(np.log1p))
joblib.dump(gsearch1, f'{seed}_lgb_gsearch1.pkl') 
open(f'{seed}_lgb_gsearch1_best_params_.txt', 'w').write(str(gsearch1.best_params_))
open(f'{seed}_lgb_gsearch1_grid_scores_.txt', 'w').write(str(gsearch1.grid_scores_))

del gsearch1

gsearch2.fit(train_data, train_data['traj_length'].apply(np.log1p))
joblib.dump(gsearch2, f'{seed}_lgb_gsearch2.pkl') 
open(f'{seed}_lgb_gsearch2_best_params_.txt', 'w').write(str(gsearch2.best_params_))
open(f'{seed}_lgb_gsearch2_grid_scores_.txt', 'w').write(str(gsearch2.grid_scores_))

import shutil
shutil.rmtree(cachedir)
