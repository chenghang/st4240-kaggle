#invite people for the Kaggle party
import pandas as pd
import numpy as np
import random
from scipy.stats import norm
from sklearn.preprocessing import StandardScaler
from sklearn.metrics.pairwise import paired_distances
from scipy import stats
import warnings
from pandas.plotting import scatter_matrix

import xgboost as xgb
from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA
from sklearn.cluster import MiniBatchKMeans
import warnings
import time
from sklearn.neighbors import NearestNeighbors, BallTree
from sklearn.decomposition import PCA
from sklearn.model_selection import KFold
from sklearn.decomposition import PCA
from sklearn.base import TransformerMixin

class Dummies(TransformerMixin):
    def __init__(self, features, **kwargs):
        self.features = features
        self.ohe = OneHotEncoder(categorical_features='all',
                                 handle_unknown='ignore')
        
    def fit(self, X, y=None):
        self.ohe.fit(X[self.features])
        return self
    
    def transform(self, X):
        return self.ohe.transform(X[self.features]).todense()
    
    def get_feature_names(self):
        return self.__feature_names

class DriverSpeed(TransformerMixin):
    def __init__(self, verbose=0, **kwargs):
        self.drv_speed = None
    
    def fit(self, df, y=None):
        drv_agg = df.groupby("taxi_id")['duration', 'traj_length'].sum()
        self.drv_speed = (drv_agg['traj_length'] / drv_agg['duration']).rename("drv_speed").reset_index()
        return self
    
    def transform(self, df):
        """transform df"""
        X = df[['taxi_id']].reset_index()\
            .merge(self.drv_speed, how='left').set_index('index')['drv_speed']
        assert all(df.index == X.index), 'Index Unaligned'
        return X.values.reshape(-1,1 )

class nnFeatures(TransformerMixin):
    def __init__(self, verbose=0, **kwargs):
        self.verbose = verbose
    
    def fit(self, df, y=None):
        nn_params=dict(
            n_neighbors=20, metric='euclidean')
        coord_values = df[['x_start', 'y_start', 'x_end', 'y_end']].values
        self.nn = NearestNeighbors(**nn_params).fit(coord_values)
        self.features_to_agg = df[['traj_length', 'curve_total', 'curve_left', 'curve_right']]
        return self
    def transform(self, df):
        verbose = self.verbose
        nn,features_to_agg = self.nn, self.features_to_agg
        coord_values = df[['x_start', 'y_start', 'x_end', 'y_end']].values
        st = time.time()
        batch_size=30000
        aggs = []
        for i in range(df.shape[0]//batch_size + 1):
            idxs=nn.kneighbors(
                coord_values[i*batch_size:min(i*batch_size+batch_size, df.shape[0])], 
                return_distance=False)
            flat_features = features_to_agg.iloc[idxs.flatten()].reset_index(drop=1)
            aggs.append(flat_features.groupby(flat_features.index//nn.n_neighbors).agg([
                'mean', 'std', 'median']))
            if verbose>1:
                print(f"{i}th group in {time.time()-st:.2f}")
        if verbose:    
            print(f"{df.shape[0]} {nn.n_neighbors}-nn queries in {time.time()-st:.2f}")
        rdf = pd.concat(aggs).reset_index(drop=1)
        rdf.columns = ['nn_{1}_{0}'.format(*col) for col in rdf.columns]
        for col in rdf.columns:
            rdf[f'log_{col}'] = np.log(rdf[col])
            del rdf[col]
#         rdf['nn_mean_simple_curature_ratio'] = rdf['nn_mean_simple_curvature']\
#             / rdf['nn_mean_traj_length'] 
        rdf.index = df.index
        assert rdf.shape[0] == df.shape[0], f'wrong output shape {rdf.shape[0]}'
        assert all(df.index == rdf.index), 'Index Unaligned'
        return rdf.values

class LatLongPca(TransformerMixin):
    def fit(self, df, y=None):
        self.pca = PCA().fit(df[['x_start', 'y_start']])
        return self
    def transform(self, df):
        start_pca = self.pca.transform(df[['x_start', 'y_start']])
        end_pca = self.pca.transform(df[['x_end', 'y_end']])
        log_pca_manh_dist = np.log1p(paired_distances(start_pca, end_pca, metric='manhattan'))
        X = pd.concat([pd.DataFrame(start_pca, columns=['pca_x_start', 'pca_y_start']),
                          pd.DataFrame(end_pca, columns=['pca_x_end', 'pca_y_end']),
                          pd.Series(log_pca_manh_dist).rename('log_pca_manh_dist')
                         ], 1)
        X.index = df.index
        assert all(df.index == X.index), 'Index Unaligned'
        return X.values

class clusterFeatures(TransformerMixin):
    def fit(self, df, y=None):
        """fit df"""
        df = df.copy()
        self.kmeans = MiniBatchKMeans(n_clusters=60, batch_size=10000).fit(
            np.vstack((df[['x_start', 'y_start']], df[['x_end', 'y_end']])))
        df['start_cluster'] = self.kmeans.predict(df[['x_start', 'y_start']])
        df['end_cluster'] = self.kmeans.predict(df[['x_end', 'y_end']])
        
        cl_agg = df.groupby(['start_cluster', 'end_cluster'])['duration', 'traj_length'].sum()
        cl_speed = (cl_agg['traj_length'] / cl_agg['duration']).rename('cl_speed').reset_index()
        cl_avg_traj = df.groupby(['start_cluster', 'end_cluster'])['traj_length'].mean()\
            .rename('cl_avg_traj_length').reset_index()
        cl_med_traj = df.groupby(['start_cluster', 'end_cluster'])['traj_length'].median()\
            .rename('cl_med_traj_length').reset_index()
        
        def cl_dist(cc):
            c1,c2=cc
            d = (self.kmeans.cluster_centers_[c1] - self.kmeans.cluster_centers_[c2])**2
            return ((d[0] + d[1])**0.5)

        cl_dist = pd.concat([
            df[['start_cluster', 'end_cluster']].drop_duplicates(), 
            df[['start_cluster', 'end_cluster']].drop_duplicates()\
                .apply(cl_dist, 1).rename('cl_dist')], 1)
        
        df = df.drop(['start_cluster', 'end_cluster'], 1)
        self.features = cl_speed.merge(cl_dist).merge(cl_avg_traj).merge(cl_med_traj)
        
    def transform(self, df):
        """transform df"""
        kmeans=self.kmeans
        clusters = pd.concat([
            pd.Series(kmeans.predict(df[['x_start', 'y_start']])),
            pd.Series(kmeans.predict(df[['x_end', 'y_end']]))],
            1, keys=['start_cluster', 'end_cluster']
        )
        clusters.index=df.index
#         start_cents = clusters['start_cluster'].map(
#             self.kmeans.cluster_centers_.__getitem__)
#         end_cents = clusters['end_cluster'].map(
#             self.kmeans.cluster_centers_.__getitem__)
#         start_cent_dist = paired_distances(list(map(list, (start_cents.values))),
#                                            df[['x_start', 'y_start']].values)
#         end_cent_dist = paired_distances(list(map(list, (start_cents.values))), 
#                                          df[['x_end', 'y_end']].values)
#         X = pd.concat([#pd.Series(start_cent_dist).rename('cl_start_cent_dist'), 
                          #pd.Series(end_cent_dist).rename('cl_end_cent_dist'),
        X =                  clusters.merge(self.features, 
                                         how='left', on=['start_cluster', 'end_cluster'])#],
#                          1)
        X.index= df.index
        assert all(df.index == X.index), 'Index Unaligned'
        return X.values

class TemporalSpeed(TransformerMixin):
    def fit(self, df, y=None):
        """fit df"""
        aggs = [
            ('date', df.groupby('date')['duration', 'traj_length'].sum()),
            ('wkhour', df.groupby(['wkday', 'hour'])['duration', 'traj_length'].sum()),
            # improve datehour to rolling window queries -- use full data with censoring -> mean with nans
            ('datehour', df.groupby(['date', 'hour'])['duration', 'traj_length'].sum()),
            ('bearing_wkhour', df.groupby(['bearing', 'wkday', 'hour']).sum())
        ]
        self.aggs = [
            (name + '_speed', agg['traj_length'].divide(agg['duration']).rename(name + '_speed').reset_index())
            for name, agg in aggs
        ]
#         self.dt_set = df[['id', 'traj_length', 'duration', 'timestamp']]
        
    def transform(self, df):
        """transform df"""
#         dt_set = pd.concat([
#             df[~df['id'].isin(self.dt_set['id'])][['id', 'traj_length', 'duration', 'timestamp']],
#             self.dt_set
#         ])
#         rolling_dt = dt_set.set_index(
#             pd.to_datetime(dt_set['timestamp']))[
#             ['id', 'traj_length', 'duration']].sort_index()
#         sorted_rolling_60min = rolling_dt.rolling('60min')
#         rolling_dt['60min_count'] = sorted_rolling_60min['id'].count()
#         rolling_dt['60min_avg_speed'] = sorted_rolling_60min[
#             'traj_length'].sum() / sorted_rolling_60min['duration'].sum()
#         rolling_dt = rolling_dt.sort_values('id')[
#             ['id', '60min_count', '60min_avg_speed']].set_index('id').reset_index(drop=1)
#         self.rolling_dt = rolling_dt
        X = pd.concat([pd.Series(df.merge(agg, how='left')[name]) 
                          for name, agg in self.aggs] 
#                          + [rolling_dt['60min_avg_speed']]
                         , 1)
        X.index = df.index
        assert all(df.index == X.index), 'Index Unaligned'
        return X.values