import sys
sys.path.append('../py')

from nb_imports import *
from model_params import model_parms
from modelling import FeatureExtract

from sklearn.externals.joblib import Memory
from tempfile import mkdtemp
# Create a temporary folder to store the transformers of the pipeline
cachedir = mkdtemp()
memory = Memory(cachedir=cachedir, verbose=0)

seed = int(str(time.time()).split('.')[1]) * 2 + int(str(time.time()).split('.')[0]) % 3
rns = [random.randint(0, 1<<31) for i in range(10)]    
seed

valid_split = KFold(n_splits=4, shuffle=True, random_state=rns[0])
test_split = KFold(n_splits=4, shuffle=True, random_state=rns[1])

train_data = pd.read_parquet('../data/train_basic3.parquet')
train_data.columns = train_data.columns.str.lower()
test_data = pd.read_parquet('../data/test_basic3.parquet')
test_data.columns = test_data.columns.str.lower()

def log_transform(x):
    return np.log1p(x)
def inv_log_transform(x):
    return np.expm1(x)

def mspe_eval(preds, dtrain, transform_f=None):
    _preds = preds
    if type(dtrain) ==  xgb.core.DMatrix:
        labels = dtrain.get_label()
    else:
        labels = dtrain
    if transform_f is not None:
        labels = transform_f(labels)
        _preds = transform_f(_preds)
    # return a pair metric_name, result
    # since preds are margin(before logistic transformation, cutoff at 0)
    return 'mspe', \
        (sum((((_preds)-(labels))/(labels)) ** 2) / len(labels))**0.5

def score_func(y, y_pred):
    return mspe_eval(y_pred, y, transform_f=inv_log_transform)[1]

mspe_scorer = make_scorer(score_func, greater_is_better=False)

def mspe_log(preds, dtrain):
    return mspe_eval(preds, dtrain, inv_log_transform)

def get_cv_scores(len_pipe, dur_pipe):
    scores = []
    for fold in test_split.split(train_data):
        len_pipe.fit(train_data.iloc[fold[0]],
                                       np.log1p(train_data['traj_length'])[fold[0]]);
        dur_pipe.fit(train_data.iloc[fold[0]],
                                       np.log1p(train_data['duration'])[fold[0]]);

        len_ptr = len_pipe.predict(train_data.iloc[fold[0]])
        dur_ptr = dur_pipe.predict(train_data.iloc[fold[0]])

        ytr_pred = inv_log_transform(len_ptr) + inv_log_transform(dur_ptr)
        ytr = train_data['traj_length'][fold[0]] + train_data['duration'][fold[0]]
        print('training_score %s' % mspe_eval(ytr_pred, ytr, transform_f=None)[1])

        len_pv = len_pipe.predict(train_data.iloc[fold[1]])
        dur_pv = dur_pipe.predict(train_data.iloc[fold[1]])
        yv_pred = inv_log_transform(len_pv) + inv_log_transform(dur_pv)
        yv = train_data['traj_length'][fold[1]] + train_data['duration'][fold[1]]
        scores.append(mspe_eval(yv_pred, yv, transform_f=None))
        print('valid_score %s' % mspe_eval(yv_pred, yv, transform_f=None)[1])

# features already excluded
do_not_use_for_training = [
    'id', 'taxi_id', 'timestamp', 'duration', 'x_start', 'y_start', 'x_end', 'y_end', 'x_trajectory', 'y_trajectory',
    'traj_length', 'straight_dist',
    'wkday', 'hour', 'month', 'date',
    'bearing',
    'curve_total', 'curve_left', 'curve_right', 'curve_total_ratio', 'curve_left_ratio', 'curve_right_ratio',
    'speed',
#     'pca_x_start', 'pca_y_start', 'pca_x_end', 'pca_y_end',
#     'hour_0', 'hour_1', 'hour_2', 'hour_3', 'hour_4', 'hour_5', 'hour_6', 'hour_7',
#     'hour_8', 'hour_9', 'hour_10', 'hour_11', 'hour_12', 'hour_13', 'hour_14', 'hour_15',
#     'hour_16', 'hour_17', 'hour_18', 'hour_19', 'hour_20', 'hour_21', 'hour_22', 'hour_23',
#     'wkday_0', 'wkday_1', 'wkday_2', 'wkday_3', 'wkday_4', 'wkday_5', 'wkday_6',
#     'month_1', 'month_2', 'month_3', 'month_4', 'month_5', 'month_6',
#     'month_7', 'month_8', 'month_9', 'month_10', 'month_11', 'month_12',
#     'bearing_-9.0', 'bearing_-8.0', 'bearing_-7.0', 'bearing_-6.0', 'bearing_-5.0', 'bearing_-4.0',
#     'bearing_-3.0', 'bearing_-2.0', 'bearing_-1.0', 'bearing_0.0', 'bearing_1.0', 'bearing_2.0',
#     'bearing_3.0', 'bearing_4.0', 'bearing_5.0', 'bearing_6.0', 'bearing_7.0', 'bearing_8.0', 'bearing_9.0',
#     'drv_speed',
#     'start_cluster', 'end_cluster', 'cl_speed', 'cl_dist', 'cl_avg_traj_length', 'cl_med_traj_length',
#     'date_speed', 'wkhour_speed', 'datehour_speed', 'bearing_wkhour_speed',
#     'log_nn_mean_traj_length', 'log_nn_std_traj_length', 'log_nn_median_traj_length',
#     'log_nn_mean_curve_total', 'log_nn_std_curve_total', 'log_nn_median_curve_total',
#     'log_nn_mean_curve_left', 'log_nn_std_curve_left', 'log_nn_median_curve_left',
#     'log_nn_mean_curve_right', 'log_nn_std_curve_right', 'log_nn_median_curve_right',
#     'log_straight_dist', 'log_cl_dist', 'log_cl_med_traj_length', 'log_cl_avg_traj_length',
#     'log_cl_speed', 'log_date_speed', 'log_wkhour_speed', 'log_bearing_wkhour_speed'
]

## ERF
et_len_pipe = Pipeline([
    ('fe', FeatureExtract(do_not_use_for_training, verbose=0)),
    ('imp', Imputer(strategy='median')),
    ('ss', StandardScaler()),
    ('et', ExtraTreesRegressor(n_estimators=200, max_features=0.8, min_samples_split=25,
                               min_samples_leaf=5,
                                 max_depth=50, bootstrap=True, oob_score=True, 
                               n_jobs=NUM_CORES))
], memory=memory)
et_dur_pipe = Pipeline([
    ('fe', FeatureExtract(do_not_use_for_training, verbose=0)),
    ('imp', Imputer(strategy='median')),
    ('ss', StandardScaler()),
    ('et', ExtraTreesRegressor(n_estimators=200, max_features=0.8, min_samples_split=25,
                               min_samples_leaf=5,
                                 max_depth=50, bootstrap=True, oob_score=True, 
                               n_jobs=NUM_CORES)),
], memory=memory)

## RF
rf_len_pipe = Pipeline([
    ('fe', FeatureExtract(do_not_use_for_training, verbose=0)),
#     ('ss', StandardScaler()),
    ('imp', Imputer(strategy='median')),
    ('rf', RandomForestRegressor(n_estimators=100, max_features='sqrt',
                                 max_depth=60, n_jobs=NUM_CORES))
], memory=memory)

rf_dur_pipe = Pipeline([
    ('fe', FeatureExtract(do_not_use_for_training, verbose=0)),
#     ('ss', StandardScaler()),
    ('imp', Imputer(strategy='median')),
    ('rf', RandomForestRegressor(n_estimators=100, max_features='sqrt',
                                 max_depth=50, n_jobs=NUM_CORES)),
], memory=memory)



## XGB

xgb_len_pipe = Pipeline([
    ('fe', FeatureExtract(do_not_use_for_training, verbose=0)),
    ('xgbr', xgb.XGBRegressor(**model_parms['len_xgb'])),
], memory=memory)
xgb_dur_pipe = Pipeline([
    ('fe', FeatureExtract(do_not_use_for_training, verbose=0)),
    ('xgbr', xgb.XGBRegressor(**model_parms['dur_xgb'])),
], memory=memory)

# boosters = []
# for fold in test_split.split(train_data):
#     fe = FeatureExtract(do_not_use_for_training, verbose=0)
#     fe.fit(train_data.iloc[fold[0]]);
#     Xtr = fe.transform(train_data.iloc[fold[0]])
#     ytr = np.log1p(train_data['traj_length'])[fold[0]]
#     Xv = fe.transform(train_data.iloc[fold[1]])
#     yv = np.log1p(train_data['traj_length'])[fold[1]]
#     dtrain = xgb.DMatrix(Xtr, label=ytr)
#     dval = xgb.DMatrix(Xv, label=yv)

#     booster = xgb.train(
#         model_parms['len_xgb'], 
#         dtrain,
#         num_boost_round=80,
#         evals=([dtrain, 'tr'], [dval, 'v']), 
#         obj=None,
#         feval=mspe_log,
#         early_stopping_rounds=10,
#         verbose_eval=True, xgb_model=None, callbacks=None, learning_rates=None)
#     boosters.append(booster)

class Ensemble(BaseEstimator, TransformerMixin, RegressorMixin):
    def __init__(self, predictor, **kwargs):
        self.predictor = predictor
        if predictor == 'dur':
            self.xgb = xgb_dur_pipe
            self.et = et_dur_pipe
        else:
            self.xgb = xgb_len_pipe
            self.et = et_len_pipe
    
    def fit(self, X, y):
        self.et.fit(X, y)
        self.xgb.fit(X, y)
        return self
        
    def predict(self, X):
        y1 = self.et.predict(X)
        y2 = self.xgb.predict(X)
        return (y1 + y2) / 2
    
    def get_params(self, **kwargs):
        return {'predictor': self.predictor}

get_cv_scores(Ensemble('len'), Ensemble('dur'))

len_pipe = Ensemble('len').fit(train_data, np.log1p(train_data['traj_length']))
dur_pipe = Ensemble('dur').fit(train_data, np.log1p(train_data['duration']))

for col in list(train_data.columns):
    if col not in test_data:
        print(col)
        test_data[col] = 0

len_ptr = len_pipe.predict(test_data[train_data.columns])
dur_ptr = dur_pipe.predict(test_data[train_data.columns])
ytr_pred = inv_log_transform(len_ptr) + inv_log_transform(dur_ptr)
pd.DataFrame(ytr_pred, index=test_data['id'], columns=['PRICE']).to_csv('../predict/p15.csv')
pd.read_csv('../predict/p14.csv', index_col='id').max()