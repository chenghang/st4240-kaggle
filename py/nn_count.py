import pandas as pd
import numpy as np

import concurrent.futures
import multiprocessing
import ctypes
from scipy.spatial.distance import cdist

train_data = pd.read_csv('../data/train_basic.csv')
train_data.columns = train_data.columns.str.lower()
test_data = pd.read_csv('../data/test_basic.csv')
test_data.columns = test_data.columns.str.lower()

all_trips = pd.concat([train_data[['id', 'timestamp', 'x_start', 'y_start', 'x_end', 'y_end']],
           test_data[['id', 'timestamp', 'x_start', 'y_start', 'x_end', 'y_end']]])
all_trips['timestamp'] = pd.to_datetime(all_trips['timestamp']).astype(int) // 1000000000
tv = all_trips.values

del train_data
del test_data
del all_trips
    
arrs = multiprocessing.Array(ctypes.c_double, 202026389*9)
shared_array = np.ctypeslib.as_array(arrs.get_obj())
shared_array = shared_array.reshape(202026389, 9)

print('Allocated shared array')

names = ['id', 'segment', 'num_segments', 'timestamp', 'x_start', 'y_start', 'x_end', 'y_end', 'segment_traj_length']
first_loc = 0
for i in range(47):
#     df = pd.read_csv('../features/path_expand/traj_data_{:03d}.csv'.format(i), names=names)
    df = pd.read_parquet('../features/path_expand/traj_data_{:03d}.parquet'.format(i))
    shared_array[first_loc:first_loc+df.values.shape[0],:] = df.values
    first_loc += df.values.shape[0]
   
print('loaded traj_data')

def query_nn_count(row, arrs=shared_array):
    ts = row[1] 
    locs = row[2:]
    _id = row[0]

    qr = arrs[np.where((arrs[:,3] > ts - 1800) & (arrs[:,3] < ts + 1800) & (arrs[:,0] != _id))]
    
    if qr.shape[0] == 0:
        return [_id, 0] + [0 for i in range(5, 51, 5)] 

    dists = cdist([locs[:2]], qr[:,4:6])[0]

    order = np.lexsort([
        qr[:,0],
        dists,
    ])

    index = np.empty(len(qr), 'bool')

    index[0] = True
    index[1:] = qr[:,0][order][1:] != qr[:,0][order][:-1]
    distsr = dists[index]
    return [_id, distsr.shape[0]] + [distsr.__lt__(i).sum() for i in range(5,51,5)]

if __name__ == '__main__':
    import time
    st = time.time()
    
    rvs = []
    with concurrent.futures.ProcessPoolExecutor(max_workers=15) as executor:
        for rv in executor.map(query_nn_count, tv):
            rvs.append(','.join(map(str,rv)) + '\n')
            if (rv[0] % 1000 == 0): 
                print(rv[0], round((time.time() - st)/60, 1))
                with open('../features/path_expand/nn_count.csv', 'a') as f:
                    f.write(''.join(rvs))
                rvs = []
                import gc;gc.collect()
    with open('../features/path_expand/nn_count.csv', 'a') as f:
        f.write(''.join(rvs))
