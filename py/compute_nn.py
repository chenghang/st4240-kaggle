import os
import time
import pandas as pd
import numpy as np
from sklearn.neighbors import BallTree

def start_end_dist(a, b):
    return ((a[0]-b[0])**2 + (a[1]-b[1])**2)**2 + ((a[2]-b[2])**2 + (a[3]-b[3])**2)**2 

if __name__ == '__main__':
    
    CWD = os.environ.get('ST4240_HOME', '/Users/Hang/st4240')
    VERSION = 1
    nn_metric = 'euclidean' 
    train = pd.read_csv(f'{CWD}/train_data.csv')
    train.columns = train.columns.str.lower()
    test = pd.read_csv(f'{CWD}/test.csv')
    test.columns = test.columns.str.lower()

    coord_values = train[['x_start', 'y_start', 'x_end', 'y_end']].values.copy()
    del train

    nn = BallTree(coord_values, metric=nn_metric)
    
    st = time.time()
    for i in range(coord_values.shape[0]//1000 + 1):
        idxs=nn.query(coord_values[i:i+1000], k=200, return_distance=False)
        with open(f'{CWD}/features/nn/balltree_train_{VERSION}.txt', 'a') as f:
            f.write(pd.DataFrame(idxs).to_csv(header=None, index=False))
        
        print(f"train Group {i} completed in {time.time() - st}")
        
    coord_values = test[['x_start', 'y_start', 'x_end', 'y_end']].values.copy()
    del test
       
    st = time.time()
    for i in range(coord_values.shape[0]//1000 + 1):
        idxs=nn.query(coord_values[i:i+1000], k=200, return_distance=False)
        with open(f'{CWD}/features/nn/balltree_test_{VERSION}.txt', 'a') as f:
            f.write(pd.DataFrame(idxs).to_csv(header=None, index=False))
        
        print(f"test Group {i} completed in {time.time() - st}")
