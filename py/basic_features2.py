import pandas as pd
import numpy as np

from sklearn.decomposition import PCA
from sklearn.metrics.pairwise import paired_distances


if __name__ == '__main__':
    train_data = pd.read_csv('../data/train_basic.csv')
    train_data.columns = train_data.columns.str.lower()
    test_data = pd.read_csv('../data/test_basic.csv')
    test_data.columns = test_data.columns.str.lower()

    pca = PCA(svd_solver='full').fit(
    np.concatenate([
        train_data[['x_start', 'y_start']],
        train_data[['x_end', 'y_end']],
        test_data[['x_start', 'y_start']],
        test_data[['x_end', 'y_end']]
    ])
    )

    train_data[['pca_x_start', 'pca_y_start']] = pd.DataFrame(pca.transform(train_data[['x_start', 'y_start']]))
    train_data[['pca_x_end', 'pca_y_end']] = pd.DataFrame(pca.transform(train_data[['x_end', 'y_end']]))
    test_data[['pca_x_start', 'pca_y_start']] = pd.DataFrame(pca.transform(test_data[['x_start', 'y_start']]))
    test_data[['pca_x_end', 'pca_y_end']] = pd.DataFrame(pca.transform(test_data[['x_end', 'y_end']]))
    
    train_data['pca_mdist'] = np.log1p(paired_distances(pd.DataFrame(pca.transform(train_data[['x_start', 'y_start']])), 
                                                  pd.DataFrame(pca.transform(train_data[['x_end', 'y_end']])),
                                                  metric='manhattan'))
    test_data['pca_mdist'] = np.log1p(paired_distances(pd.DataFrame(pca.transform(test_data[['x_start', 'y_start']])), 
                                              pd.DataFrame(pca.transform(test_data[['x_end', 'y_end']])),
                                              metric='manhattan'))
    
    train_data['euclidcut'] = train_data['traj_length'] / train_data['straight_dist']
    train_data['manhcut'] = train_data['traj_length'] / train_data['pca_mdist']
    
    train_data['date'] = (pd.to_datetime(train_data['date']) - pd.to_datetime(train_data['date']).min()).astype('timedelta64[D]').astype(int)
    
    test_data['date'] = (pd.to_datetime(test_data['date']) - pd.to_datetime(test_data['date']).min()).astype('timedelta64[D]').astype(int)
    
    train_data.loc[:, 'wkhour'] = train_data['wkday'] * 24 + train_data['hour']
    test_data.loc[:, 'wkhour'] = test_data['wkday'] * 24 + train_data['hour']
    
    train_data.to_parquet('../data/train_basic2.parquet')
    test_data.to_parquet('../data/test_basic2.parquet')
    
    for col in ['x_trajectory', 'y_trajectory', 'timestamp']:
        if col in train_data:
            del train_data[col]
        if col in test_data:
            del test_data[col]
        
    train_data.to_parquet('../data/train_basic3.parquet')
    test_data.to_parquet('../data/test_basic3.parquet')

