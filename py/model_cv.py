import sys
sys.path.append('../py')

from nb_imports import *
from model_params import model_parms
from modelling import FeatureExtract, do_not_use_for_training

from sklearn.externals.joblib import Memory
from tempfile import mkdtemp
# Create a temporary folder to store the transformers of the pipeline
cachedir = mkdtemp(dir='/mnt/temp')
memory = Memory(cachedir=cachedir, verbose=0)

import lightgbm as lgb

seed = int(str(time.time()).split('.')[1]) * 2 + int(str(time.time()).split('.')[0]) % 3
rns = [random.randint(0, 1<<31) for i in range(10)]    
seed

valid_split = KFold(n_splits=4, shuffle=True, random_state=rns[0])
test_split = KFold(n_splits=4, shuffle=True, random_state=rns[1])

train_data = pd.read_parquet('../data/train_basic3.parquet')
train_data.columns = train_data.columns.str.lower()
test_data = pd.read_parquet('../data/test_basic3.parquet')
test_data.columns = test_data.columns.str.lower()

def log_transform(x):
    return np.log1p(x)
def inv_log_transform(x):
    return np.expm1(x)

def mspe_eval(preds, dtrain, transform_f=None):
    _preds = preds
    if type(dtrain) ==  xgb.core.DMatrix:
        labels = dtrain.get_label()
    elif type(dtrain) ==  lgb.Dataset:
        labels = dtrain.get_label()
    else:
        labels = dtrain
    if transform_f is not None:
        labels = transform_f(labels)
        _preds = transform_f(_preds)
    # return a pair metric_name, result
    # since preds are margin(before logistic transformation, cutoff at 0)
    if type(dtrain) ==  lgb.Dataset:
        return 'mspe', \
            (sum((((_preds)-(labels))/(labels)) ** 2) / len(labels))**0.5, False
    return 'mspe', \
        (sum((((_preds)-(labels))/(labels)) ** 2) / len(labels))**0.5

def score_func(y, y_pred):
    return mspe_eval(y_pred, y, transform_f=inv_log_transform)[1]

mspe_scorer = make_scorer(score_func, greater_is_better=False)

def mspe_log(preds, dtrain):
    return mspe_eval(preds, dtrain, inv_log_transform)

from sklearn.externals import joblib

def get_cv_scores(len_pipe, dur_pipe):
    scores = []
    for fold in test_split.split(train_data):
        len_pipe.fit(train_data.iloc[fold[0]],
                                       np.log1p(train_data['traj_length'])[fold[0]]);
        dur_pipe.fit(train_data.iloc[fold[0]],
                                       np.log1p(train_data['duration'])[fold[0]]);

        len_ptr = len_pipe.predict(train_data.iloc[fold[0]])
        dur_ptr = dur_pipe.predict(train_data.iloc[fold[0]])

        ytr_pred = inv_log_transform(len_ptr) + inv_log_transform(dur_ptr)
        ytr = train_data['traj_length'][fold[0]] + train_data['duration'][fold[0]]
        print('training_score %s' % mspe_eval(ytr_pred, ytr, transform_f=None)[1])

        len_pv = len_pipe.predict(train_data.iloc[fold[1]])
        dur_pv = dur_pipe.predict(train_data.iloc[fold[1]])
        yv_pred = inv_log_transform(len_pv) + inv_log_transform(dur_pv)
        yv = train_data['traj_length'][fold[1]] + train_data['duration'][fold[1]]
        scores.append(mspe_eval(yv_pred, yv, transform_f=None))
        print('valid_score %s' % mspe_eval(yv_pred, yv, transform_f=None)[1])

## ERF
et_len_pipe = Pipeline([
    ('fe', FeatureExtract(do_not_use_for_training, verbose=0, training=True)),
    ('imp', Imputer(strategy='median')),
    ('ss', StandardScaler()),
    ('et', ExtraTreesRegressor(n_estimators=200, max_features=0.8, min_samples_split=25,
                               min_samples_leaf=5,
                                 max_depth=50, bootstrap=True, oob_score=True, 
                               n_jobs=NUM_CORES))
], memory=memory)
et_dur_pipe = Pipeline([
    ('fe', FeatureExtract(do_not_use_for_training, verbose=0, training=True)),
    ('imp', Imputer(strategy='median')),
    ('ss', StandardScaler()),
    ('et', ExtraTreesRegressor(n_estimators=200, max_features=0.8, min_samples_split=25,
                               min_samples_leaf=5,
                                 max_depth=50, bootstrap=True, oob_score=True, 
                               n_jobs=NUM_CORES)),
], memory=memory)

## RF
rf_len_pipe = Pipeline([
    ('fe', FeatureExtract(do_not_use_for_training, verbose=0, training=True)),
#     ('ss', StandardScaler()),
    ('imp', Imputer(strategy='median')),
    ('rf', RandomForestRegressor(n_estimators=100, max_features='sqrt',
                                 max_depth=60, n_jobs=NUM_CORES))
], memory=memory)

rf_dur_pipe = Pipeline([
    ('fe', FeatureExtract(do_not_use_for_training, verbose=0, training=True)),
#     ('ss', StandardScaler()),
    ('imp', Imputer(strategy='median')),
    ('rf', RandomForestRegressor(n_estimators=100, max_features='sqrt',
                                 max_depth=50, n_jobs=NUM_CORES)),
], memory=memory)



## XGB

xgb_len_pipe = Pipeline([
    ('fe', FeatureExtract(do_not_use_for_training, verbose=0, training=True)),
    ('xgbr', xgb.XGBRegressor(**model_parms['len_xgb'])),
], memory=memory)
xgb_dur_pipe = Pipeline([
    ('fe', FeatureExtract(do_not_use_for_training, verbose=0, training=True)),
    ('xgbr', xgb.XGBRegressor(**model_parms['dur_xgb'])),
], memory=memory)

# LGB

lgb_len_pipe = Pipeline([
    ('fe', FeatureExtract(do_not_use_for_training, verbose=0)),
    ('xgbr', lgb.LGBMRegressor()),
], memory=memory)
lgb_dur_pipe = Pipeline([
    ('fe', FeatureExtract(do_not_use_for_training, verbose=0)),
    ('xgbr', lgb.LGBMRegressor()),
], memory=memory)


class Ensemble(BaseEstimator, TransformerMixin, RegressorMixin):
    def __init__(self, predictor, training=True, **kwargs):
        self.predictor = predictor
        if predictor == 'dur':
            self.xgb = xgb_dur_pipe
            self.et = et_dur_pipe
        else:
            self.xgb = xgb_len_pipe
            self.et = et_len_pipe
    
    def fit(self, X, y):
        self.et.fit(X, y)
        self.xgb.fit(X, y)
        return self
        
    def predict(self, X):
        y1 = self.et.predict(X)
        y2 = self.xgb.predict(X)
        return (y1 + y2) / 2
    
    def get_params(self, **kwargs):
        return {'predictor': self.predictor}

lgb_len_params = {'boosting_type': 'gbdt',
 'class_weight': None,
 'colsample_bytree': 1.0,
 'learning_rate': 0.1,
 'max_depth': -1,
 'min_child_samples': 20,
 'min_child_weight': 0.001,
 'min_split_gain': 0.0,
 'n_estimators': 200,
 'n_jobs': -1,
 'num_leaves': 31,
 'objective': 'regression',
 'random_state': None,
 'reg_alpha': 0.0,
 'reg_lambda': 0.0,
 'silent': False,
 'subsample': 1.0,
 'subsample_for_bin': 200000,
 'subsample_freq': 1}

scores = get_cv_scores(Ensemble('len'), Ensemble('dur'))

print(scores)
open(f'../{seed}cv_scores.txt', 'w').write(str(scores))