#invite people for the Kaggle party
import numpy as np
import pandas as pd
from derived_features import *
from sklearn.base import TransformerMixin, BaseEstimator

class FeatureExtract(TransformerMixin, BaseEstimator):
    def __init__(self, do_not_use_for_training=None, include=None, verbose=0, dummies=False, **kwargs):
        self.verbose = verbose
        if include is None:
            self.include = ['cluster', 'nn', 'speed']
        else:
            self.include = list(include)
        if do_not_use_for_training:
            self.do_not_use_for_training = do_not_use_for_training
        else:
            self.do_not_use_for_training = []
#         self.dummies = dummies

    def fit(self, df, y=None):
        verbose = self.verbose
        if verbose:
            print(f"shape of training set {df.shape}")
        self.verbose = verbose
        if 'speed' in self.include:
            self.driver_features = DriverSpeed(df)
            self.temporalspeed_features = TemporalSpeed(df)
        if 'cluster' in self.include:
            self.cluster_features = clusterFeatures(df)
        if 'nn' in self.include:
            self.nn_features = nnFeatures(df)
#         if self.dummies:
#             self.dummy_cols = pd.get_dummies(df, columns=['hour', 'wkday', 'month', 'bearing']).columns
        return self
        
    def transform(self, df):
        if self.verbose:
            print(f"shape of transformed set {df.shape}")
#         if self.dummies:
#             X = pd.get_dummies(df, columns=['hour', 'wkday', 'month', 'bearing'])
#             for col in self.dummy_cols:
#                 if col not in X:
#                     X[col] = 0
#             feature_sets = [X[self.dummy_cols]]
        feature_sets = [df]
        if 'speed' in self.include:
            feature_sets += [self.driver_features(df),  self.temporalspeed_features(df)]
        if 'cluster' in self.include:
            feature_sets.append(self.cluster_features(df))
        if 'nn' in self.include:
            feature_sets.append(self.nn_features(df, verbose=self.verbose))
        X = pd.concat(feature_sets, 1)
        for col in ['straight_dist', 'cl_dist', 'pca_mdist' #'cl_med_traj_length', 'cl_avg_traj_length',
#                    'cl_start_cent_dist', 'cl_end_cent_dist'
#                      'cl_speed', 'date_speed', 'wkhour_speed', 'bearing_wkhour_speed',
                   ]:
            if col in X.columns:
                X[f'log_{col}'] = np.log1p(X[col])
        for f in self.do_not_use_for_training:
            if f in X:
                del X[f]
        assert all(df.index == X.index), 'Index Unaligned'
        return X