import multiprocessing
NUM_CORES = multiprocessing.cpu_count()

# features already excluded
do_not_use_for_training = [
    'id', 'taxi_id', 'timestamp', 'duration', 'x_start', 'y_start', 'x_end', 'y_end', 'x_trajectory', 'y_trajectory',
    'traj_length', 'straight_dist',
#     'wkday', 'hour', 
    'month', 'date',
    'bearing',
    'curve_total', 'curve_left', 'curve_right', 'curve_total_ratio', 'curve_left_ratio', 'curve_right_ratio',
    'speed',
#     'pca_x_start', 'pca_y_start', 'pca_x_end', 'pca_y_end',
#     'hour_0', 'hour_1', 'hour_2', 'hour_3', 'hour_4', 'hour_5', 'hour_6', 'hour_7',
#     'hour_8', 'hour_9', 'hour_10', 'hour_11', 'hour_12', 'hour_13', 'hour_14', 'hour_15',
#     'hour_16', 'hour_17', 'hour_18', 'hour_19', 'hour_20', 'hour_21', 'hour_22', 'hour_23',
#     'wkday_0', 'wkday_1', 'wkday_2', 'wkday_3', 'wkday_4', 'wkday_5', 'wkday_6',
#     'month_1', 'month_2', 'month_3', 'month_4', 'month_5', 'month_6',
#     'month_7', 'month_8', 'month_9', 'month_10', 'month_11', 'month_12',
#     'bearing_-9.0', 'bearing_-8.0', 'bearing_-7.0', 'bearing_-6.0', 'bearing_-5.0', 'bearing_-4.0',
#     'bearing_-3.0', 'bearing_-2.0', 'bearing_-1.0', 'bearing_0.0', 'bearing_1.0', 'bearing_2.0',
#     'bearing_3.0', 'bearing_4.0', 'bearing_5.0', 'bearing_6.0', 'bearing_7.0', 'bearing_8.0', 'bearing_9.0',
#     'drv_speed',
    'start_cluster', 'end_cluster', 
#     'cl_speed', 
    'cl_dist',
#     'cl_avg_traj_length', 'cl_med_traj_length',
#     'date_speed', 'wkhour_speed', 'datehour_speed', 
    'bearing_wkhour_speed',
#     'log_nn_mean_traj_length', 'log_nn_std_traj_length', 'log_nn_median_traj_length',
#     'log_nn_mean_curve_total', 'log_nn_std_curve_total', 'log_nn_median_curve_total',
#     'log_nn_mean_curve_left', 'log_nn_std_curve_left', 'log_nn_median_curve_left',
#     'log_nn_mean_curve_right', 'log_nn_std_curve_right', 'log_nn_median_curve_right',
#     'log_straight_dist', 'log_cl_dist', 'log_cl_med_traj_length', 'log_cl_avg_traj_length',
    'log_cl_speed', 'log_date_speed', 'log_wkhour_speed', 'log_bearing_wkhour_speed'
]

dur_xgb_params = {'base_score': 0.5,
 'booster': 'gbtree',
 'colsample_bylevel': 1,
 'colsample_bytree': 0.8,
 'gamma': 0,
 'learning_rate': 0.08,
 'max_delta_step': 0,
 'max_depth': 11,
 'min_child_weight': 9,
 'missing': None,
 'n_estimators': 46,
 'n_jobs': 2,
 'nthread': NUM_CORES,
 'objective': 'reg:linear',
 'random_state': 0,
 'reg_alpha': 0,
 'reg_lambda': 1,
 'scale_pos_weight': 1,
 'seed': 0,
 'silent': False,
 'subsample': 0.8}

len_xgb_params = {'base_score': 0.5,
 'booster': 'gbtree',
 'colsample_bylevel': 1,
 'colsample_bytree': 0.8,
 'gamma': 0.0,
 'learning_rate': 0.08,
 'max_delta_step': 0,
 'max_depth': 10,
 'min_child_weight': 12,
 'missing': None,
 'n_estimators': 47,
 'n_jobs': 1,
 'nthread': NUM_CORES,
 'objective': 'reg:linear',
 'random_state': 0,
 'reg_alpha': 0,
 'reg_lambda': 1,
 'scale_pos_weight': 1,
 'seed': 0,
 'silent': False,
 'subsample': 0.8}

dur_et_params = dict(
    n_estimators=160, 
    max_features=0.75,
    min_samples_split=25,
    min_samples_leaf=5,
    max_depth=50,
    bootstrap=True,
    oob_score=True,
    n_jobs=NUM_CORES,
)

len_et_params = dict(
    n_estimators=160, 
    max_features=0.8,
    min_samples_split=25,
    min_samples_leaf=5,
    max_depth=50,
    bootstrap=True,
    oob_score=True,
    n_jobs=NUM_CORES,
)

lgb_dur_params = dict(boosting_type="rf",
                 num_leaves=2000,
                 colsample_bytree=.5,
                 n_estimators=250,
                 min_child_weight=5,
                 min_child_samples=60,
                 subsample=.632, # Standard RF bagging fraction
                 subsample_freq=1,
                 min_split_gain=0,
                 reg_alpha=0, 
                 reg_lambda=0,
                 n_jobs=16, 
)

lgb_len_params = dict(boosting_type="rf",
                 num_leaves=2000,
                 colsample_bytree=.5,
                 n_estimators=250,
                 min_child_weight=5,
                 min_child_samples=25,
                 subsample=.632, # Standard RF bagging fraction
                 subsample_freq=1,
                 min_split_gain=0,
                 reg_alpha=0, 
                 reg_lambda=0,
                 n_jobs=16, 
)

model_parms = {
    'len_xgb' : len_xgb_params,
    'dur_xgb': dur_xgb_params,
    'len_et' : len_et_params,
    'dur_et': dur_et_params,
    'len_lgb' : lgb_len_params,
    'dur_lgb' : lgb_dur_params
}