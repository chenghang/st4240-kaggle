import pandas as pd
import numpy as np

import multiprocessing

import time
import gc
import concurrent.futures
import multiprocessing
import ctypes
from scipy.spatial.distance import cdist

train_data = pd.read_csv('../data/train_basic.csv')
train_data.columns = train_data.columns.str.lower()
test_data = pd.read_csv('../data/test_basic.csv')
test_data.columns = test_data.columns.str.lower()

all_trips = pd.concat([train_data[['id', 'timestamp', 'x_start', 'y_start', 'x_end', 'y_end']],
           test_data[['id', 'timestamp', 'x_start', 'y_start', 'x_end', 'y_end']]])
all_trips['timestamp'] = pd.to_datetime(all_trips['timestamp']).astype(int) // 1000000000
tv = all_trips.values

del train_data
del test_data
del all_trips

names = ['id', 'segment', 'timestamp', 'x_start', 'y_start']

df = pd.read_parquet('../features/path_expand/start_pos/all.parquet').sort_values('timestamp')

shared_array = np.ctypeslib.as_array(multiprocessing.Array(ctypes.c_double, df.shape[0]*df.shape[1]).get_obj())
shared_array = shared_array.reshape(df.shape[0],df.shape[1])
shared_array[:] = df.values

del df

def query_nn_count(rows, arrs=shared_array):
    rvs = []
    for row in rows:
        ts = row[1]
        locs = row[2:]
        _id = row[0]

        lo,hi = np.searchsorted(arrs[:,2], [ts - 1800, ts + 1800])
        qr = arrs[lo:hi,]

        if qr.shape[0] == 0:
            rvs.append(','.join(map(str,[_id, 0] + [0 for i in range(5, 51, 5)])) + '\n')
            continue
            
        dists = cdist([locs[:2]], qr[:,3:5])[0]

        order = np.lexsort([
            dists,
            qr[:,0],
        ])

        index = np.empty(len(qr), 'bool')

        index[0] = True
        index[1:] = qr[:,0][order][1:] != qr[:,0][order][:-1]
        distsr = dists[index]
        rvs.append(','.join(map(str, [_id, distsr.shape[0]] + [distsr.__lt__(i).sum() for i in range(5,51,5)])) + '\n')
    return rvs


st = time.time()
part = 0
with concurrent.futures.ProcessPoolExecutor(max_workers=multiprocessing.cpu_count()) as executor:
    for rv in executor.map(query_nn_count, np.array_split(tv, 128)):
        part += 1
        print(part, round((time.time() - st )/ 60, 2))
        with open('../features/path_expand/nn_count.csv', 'a') as f:
            f.write(''.join(rv))
        gc.collect()
