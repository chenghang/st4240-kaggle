#invite people for the Kaggle party
import os
import time
import multiprocessing

import numpy as np
import pandas as pd
from sklearn.cluster import MiniBatchKMeans
from sklearn.decomposition import PCA
from sklearn.metrics.pairwise import paired_distances
from sklearn.neighbors import NearestNeighbors

NUM_CORES = multiprocessing.cpu_count()

def dummies(df, dummy_features):
    return pd.concat([pd.get_dummies(df[_], prefix=_) for _ in dummy_features], 1)

class DriverSpeed:
    def __init__(self, df):
        """fit df"""
        self.drv_agg = df.groupby("taxi_id")[['speed', 'euclidcut', 'manhcut']].agg('median').reset_index()
        self.drv_agg.columns = ['taxi_id' ,'drv_speed', 'drv_euclidcut', 'drv_manhcut']
        
    def __call__(self, df):
        """transform df"""
        X = df[['taxi_id']].reset_index()\
            .merge(self.drv_agg, how='left').set_index('index')[['drv_speed', 'drv_euclidcut', 'drv_manhcut']]
        assert all(df.index == X.index), 'Index Unaligned'
        return X

class nnFeatures:
    def __init__(self, df, n_neighbors=35):
        nn_params=dict(n_jobs=max(1, NUM_CORES-1),
            n_neighbors=n_neighbors+1, metric='euclidean')
        coord_values = df[['x_start', 'y_start', 'x_end', 'y_end']].values
        self.nn = NearestNeighbors(**nn_params).fit(coord_values)
        self.features_to_agg = df[['traj_length', 'duration', 'speed',
                                   'curve_total', 'curve_left', 'curve_right']]        
        self.n_neighbours = n_neighbors
        self.trained_ids = df['id']
    def __call__(self, df, verbose=0):
        if len(df.index) != len(self.trained_ids) or all(df['id'] == self.trained_ids):
            ignore_fit=True
        else:
            ignore_fit=False
        
        nn,features_to_agg = self.nn, self.features_to_agg
        coord_values = df[['x_start', 'y_start', 'x_end', 'y_end']].values
        st = time.time()
        batch_size=30000
        aggs = []
        for i in range(df.shape[0]//batch_size + 1):
            if ignore_fit:
                idxs=nn.kneighbors(
                    coord_values[i*batch_size:min(i*batch_size+batch_size, df.shape[0])], 
                    return_distance=False)
                nns_stack = []
                for j in range(i*batch_size,min(i*batch_size+batch_size, df.shape[0])):
                    nns_stack.append(idxs[j-i*batch_size][idxs[j-i*batch_size] != j][:self.n_neighbours])
            else:
                idxs=nn.kneighbors(
                    coord_values[i*batch_size:min(i*batch_size+batch_size, df.shape[0])], 
                    return_distance=False)
                nns_stack = []
                for idx_row in idxs:
                    nns_stack.append(idx_row[:self.n_neighbours])
            idxs = np.hstack(nns_stack)
            flat_features = features_to_agg.iloc[idxs.flatten()].reset_index(drop=1)
            aggs.append(flat_features.groupby(flat_features.index//self.n_neighbours).agg([
                'min', 'std', 'median']))
            if verbose>1:
                print(f"{i}th group in {time.time()-st:.2f}")
        if verbose:    
            print(f"{df.shape[0]} {nn.n_neighbors}-nn queries in {time.time()-st:.2f}")
        rdf = pd.concat(aggs).reset_index(drop=1)
        rdf.columns = ['nn_{1}_{0}'.format(*col) for col in rdf.columns]
        for col in rdf.columns:
            rdf[f'log_{col}'] = np.log1p(rdf[col])
            del rdf[col]
#         rdf['nn_mean_simple_curature_ratio'] = rdf['nn_mean_simple_curvature']\
#             / rdf['nn_mean_traj_length'] 
        rdf.index = df.index
        assert rdf.shape[0] == df.shape[0], f'wrong output shape {rdf.shape[0]}'
        assert all(df.index == rdf.index), 'Index Unaligned'
        return rdf


class clusterFeatures:
    def __init__(self, df):
        """fit df"""
        self.agg_cols = []
        
        cl_agg_end = df.groupby(['start_cluster', 'end_cluster'])['duration', 'traj_length', 'speed'].agg(['mean', 'median', 'min'])
        cl_agg_end = np.log1p(cl_agg_end)
        cl_agg_end.columns = ['log_cluster_%s_%s' %x for x in cl_agg_end.columns]
        cl_agg_end['cluster_count'] = df.groupby(['start_cluster', 'end_cluster'])[['id']].count()
        cl_agg_end = cl_agg_end[cl_agg_end['cluster_count'].gt(5)]
        self.agg_cols += list(cl_agg_end.columns)
        cl_agg_end = cl_agg_end.reset_index()
        del cl_agg_end['cluster_count']
        self.agg_cols.remove('cluster_count')

        cl_agg_hr = df.groupby(['start_cluster', 'hour'])['duration', 'traj_length', 'speed'].agg(['mean', 'median', 'min'])
        cl_agg_hr = np.log1p(cl_agg_hr)
        cl_agg_hr.columns = ['log_clusterhr_%s_%s' %x for x in cl_agg_hr.columns]
        cl_agg_hr['clusterhr_count'] = df.groupby(['start_cluster', 'hour'])[['id']].count()
        self.agg_cols += list(cl_agg_hr.columns)
        cl_agg_hr = cl_agg_hr.reset_index()
        cl_agg_hr = cl_agg_hr[cl_agg_hr['clusterhr_count'].gt(5)]
        del cl_agg_hr['clusterhr_count']
        self.agg_cols.remove('clusterhr_count')
        
        self.cl_agg_hr = cl_agg_hr
        self.cl_agg_end = cl_agg_end
        
    def __call__(self, df):
        """transform df"""
        X = df.reset_index().merge(
            self.cl_agg_end, how='left').merge(
            self.cl_agg_hr, how='left').set_index('index')[self.agg_cols]
        assert all(df.index == X.index), 'Index Unaligned'
        return X

class TemporalSpeed():
    def __init__(self, df):
        """fit df"""
        aggs = [
            ('date', df.groupby('date')['speed'].mean()),
            ('wkhour', df.groupby(['wkhour'])['speed'].mean()),
            # improve datehour to rolling window queries -- use full data with censoring -> mean with nans
            ('datehour', df.groupby(['date', 'hour'])['speed'].mean()),
            ('bearing_wkhour', df.groupby(['bearing', 'wkhour'])['speed'].mean())
        ]
        self.aggs = [
            (name + '_speed', agg.rename(name + '_speed').reset_index())
            for name, agg in aggs
        ]
#         self.dt_set = df[['id', 'traj_length', 'duration', 'timestamp']]
        
    def __call__(self, df):
        """transform df"""
#         dt_set = pd.concat([
#             df[~df['id'].isin(self.dt_set['id'])][['id', 'traj_length', 'duration', 'timestamp']],
#             self.dt_set
#         ])
#         rolling_dt = dt_set.set_index(
#             pd.to_datetime(dt_set['timestamp']))[
#             ['id', 'traj_length', 'duration']].sort_index()
#         sorted_rolling_60min = rolling_dt.rolling('60min')
#         rolling_dt['60min_count'] = sorted_rolling_60min['id'].count()
#         rolling_dt['60min_avg_speed'] = sorted_rolling_60min[
#             'traj_length'].sum() / sorted_rolling_60min['duration'].sum()
#         rolling_dt = rolling_dt.sort_values('id')[
#             ['id', '60min_count', '60min_avg_speed']].set_index('id').reset_index(drop=1)
#         self.rolling_dt = rolling_dt
        X = pd.concat([pd.Series(df.merge(agg, how='left')[name]) 
                          for name, agg in self.aggs] 
#                          + [rolling_dt['60min_avg_speed']]
                         , 1)
        X.index = df.index
        assert all(df.index == X.index), 'Index Unaligned'
        return X