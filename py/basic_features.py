import pandas as pd
import time
import numpy as np
import math
import itertools

def time_features(df):
    wk_day = pd.to_datetime(df['timestamp']).dt.dayofweek
    hour = pd.to_datetime(df['timestamp']).dt.hour
    month = pd.to_datetime(df['timestamp']).dt.month
    date = pd.to_datetime(df['timestamp']).dt.date
    return pd.concat([wk_day, hour, month, date], 1, keys=['wkday', 'hour', 'month', 'date'])

def manhatten_dist(df):
    return (((df['x_start'] - df['x_end']) ** 2 + (df['y_start'] + df['y_end']) ** 2) ** 0.5).rename('straight_dist')

def bearing_array(df, bin_size=20):
    lat1, lng1, lat2, lng2 = df['x_start'], df['y_start'], df['x_end'], df['y_end']
    """ function was taken from beluga's notebook as this function works on array
    while my function used to work on individual elements and was noticably slow"""
    avg_EARTH_RADIUS = 6371  # in km
    lng_delta_rad = np.radians(lng2 - lng1)
    lat1, lng1, lat2, lng2 = map(np.radians, (lat1, lng1, lat2, lng2))
    y = np.sin(lng_delta_rad) * np.cos(lat2)
    x = np.cos(lat1) * np.sin(lat2) - np.sin(lat1) * np.cos(lat2) * np.cos(lng_delta_rad)
    bearing = np.degrees(np.arctan2(y, x)).rename("bearing")
    return bearing//bin_size

def batch_curvature(grp):
    def norm(a):
        return (a[0]**2 + a[1]**2)**0.5
    
    def diff(a, b):
        """ == a - b """
        return (a[0] - b[0], a[1] - b[1])
        
    def simple_curvature(xsys):
        xs,ys = xsys
        xs,ys = np.vectorize(int)(xs.split(',')),np.vectorize(int)(ys.split(','))
        total = 0.0
        left = 0.0
        right = 0.0
        for i in range(len(xs)-2):
            ps = [(xs[j],ys[j]) for j in range(i,i+3)]
            a = diff(ps[1], ps[0])
            b = diff(ps[2], ps[1])
            c = diff(ps[2], ps[0])
            cross = a[0]*b[1] - a[1]*b[0]
            na,nb = norm(a),norm(b)
            if cross > 0.0:
                left += cross / (na + nb)
            if cross < 0.0:
                right += -cross / (na + nb)
            total += (na + nb -norm(c))
        return (total, left, right, total/len(xs), left/len(xs), right/len(xs))
    
    rv = list(map(simple_curvature, grp))
    return rv

def curvature_feature(df):
    import concurrent.futures
    import multiprocessing
    st = time.time()
    xsys = train[['x_trajectory', 'y_trajectory']].values
    batch_size = 25000
    output = []
    batch_id = 0
    batches = xsys.shape[0]//batch_size + 1
    with concurrent.futures.ProcessPoolExecutor(max_workers=multiprocessing.cpu_count()) as executor:
        for rv in executor.map(
            batch_curvature, 
            (xsys[grp*batch_size:min(grp*batch_size+batch_size, xsys.shape[0])] 
             for grp in range(batches))
        ):
            # put results into correct output list
            batch_id+=1
            print(f"batch {batch_id} received in {time.time()-st:.2f}")
            output.append(rv)
    print(f"{time.time()-st:.2f}s taken")
    output = list(itertools.chain.from_iterable(output))
    return pd.DataFrame(
        output, 
        columns=['curve_total', 'curve_left','curve_right',
                 'curve_total_ratio', 'curve_left_ratio', 'curve_right_ratio'])

def speed(df):
    return (df['traj_length'] / df['duration']).rename('speed')


# we use these "basic" features
def basic_features(df):
    return pd.concat([
    df,
    manhatten_dist(df),
    time_features(df),
    bearing_array(df)],
    1)

# for deriving more features on training
def basic_train_features(df):
    return pd.concat([
        df,
        curvature_feature(df),
        speed(df)], 1)


if __name__ == '__main__':
    import os
    CWD = os.environ.get('ST4240_HOME', '/Users/Hang/st4240')
    
    
    train = pd.read_csv(f'{CWD}/train_data.csv')
    train.columns = train.columns.str.lower()
    test = pd.read_csv(f'{CWD}/test_data.csv')
    test.columns = test.columns.str.lower()

    train_bs = basic_features(train)
    train_bs = basic_train_features(train_bs)
    train_bs.columns = train_bs.columns.str.upper()
    train_bs.to_csv(f'{CWD}/data/train_basic.csv', index=False)
    test_bs = basic_features(test)
    test_bs.columns = test_bs.columns.str.upper()
    test_bs.to_csv(f'{CWD}/data/test_basic.csv', index=False)