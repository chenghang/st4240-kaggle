#invite people for the Kaggle party
import os
import sys
import multiprocessing
import random
import warnings
import time
import shutil

import numpy as np
import pandas as pd

from scipy import stats
from scipy.stats import norm

from sklearn.base import BaseEstimator, TransformerMixin, RegressorMixin, clone
from sklearn.preprocessing import StandardScaler, RobustScaler, Imputer
from sklearn.metrics import mean_squared_error, make_scorer
from sklearn.metrics.pairwise import paired_distances
from sklearn.model_selection import KFold, cross_val_score, train_test_split, GridSearchCV, ShuffleSplit
from sklearn.pipeline import make_pipeline, Pipeline

from sklearn.linear_model import ElasticNetCV, LassoCV, BayesianRidge, LassoLarsCV, RidgeCV
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor, ExtraTreesRegressor
from sklearn.kernel_ridge import KernelRidge
from sklearn.decomposition import PCA
from sklearn.cluster import MiniBatchKMeans
from sklearn.neighbors import NearestNeighbors, BallTree

import lightgbm as lgb
import xgboost as xgb

NUM_CORES = multiprocessing.cpu_count()
original_path = sys.path