import pandas as pd
from scipy.spatial.distance import euclidean

def get_traj_parts(data):
    xs = list(map(int,data[1].split(', ')))
    ys = list(map(int,data[2].split(', ')))
    ts = data[4]

    dists = [0]
    acc = 0
    for i in range(len(xs)-1):
        acc += euclidean([xs[i],ys[i]], [xs[i+1],ys[i+1]])
        dists.append(acc)

    _id = data[0]

    res = []
    for i in range(len(xs)):
        for j in range(i+19, len(xs)):
            res.append(','.join(map(str,(
                _id, i, j-i, ts+6*(j-i),
                xs[i], ys[i], xs[j], ys[j], 
                round(dists[j] - dists[i],4)
            ))) + '\n')
    return ''.join(res)


train_data = pd.read_csv('../data/train_basic.csv')
train_data.columns = train_data.columns.str.lower()
# test_data = pd.read_csv('../data/test_basic.csv')
# test_data.columns = test_data.columns.str.lower()

traj_data = train_data[['id', 'x_trajectory', 'y_trajectory', 'traj_length', 'timestamp']]
traj_data['timestamp'] = pd.to_datetime(traj_data['timestamp']).astype(int) // 1000000000
traj_data = traj_data.values

n = 1
for x in map(get_traj_parts, traj_data):
    with open('../features/path_expand/traj_data_{:03d}.csv'.format(n // 10000), 'a') as f:
        f.write(x)
        if (n % 10000 == 0): print(n)
        n += 1
   